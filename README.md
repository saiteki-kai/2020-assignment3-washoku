# Assignment 3 - Washoku

[https://gitlab.com/saiteki-kai/2020-assignment3-washoku](https://gitlab.com/saiteki-kai/2020-assignment3-washoku)

[![Spring](https://img.shields.io/badge/spring%20-%236DB33F.svg?&style=for-the-badge&logo=spring&logoColor=white)](https://spring.io/)
[![Maven](https://img.shields.io/badge/3.6.3%20-%23C71A36.svg?&label=maven&style=for-the-badge&logo=apache-maven&logoColor=white)](https://maven.apache.org/)
[![Java](https://img.shields.io/badge/11%20-%23007396.svg?&label=java&style=for-the-badge&logo=java&logoColor=white)](https://www.java.com/)
[![Thymeleaf](https://img.shields.io/badge/thymeleaf%20-%23005F0F.svg?&label=&style=for-the-badge&logo=bulma&logoColor=white)](https://www.thymeleaf.org/)
[![Codacy grade](https://img.shields.io/codacy/grade/0f3551869da346dd8f5edf0dcc6a57d8?logo=codacy&style=for-the-badge)](https://www.codacy.com?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=saiteki-kai/2020-assignment3-washoku&amp;utm_campaign=Badge_Grade)

<!--[![Bulma](https://img.shields.io/badge/bulma%20-%2300d1ad.svg?&label=&style=for-the-badge&logo=bulma&logoColor=white)](https://bulma.io/documentation/)-->

## Group

- Mirco Malanchini; 829889 (mircomalanchini@hotmai.it)
- Gaetano Magazzù; 829685 (lordfighterx@gmail.com, gaetanomagazzu3@gmail.com)
- Giuseppe Magazzù; 829612 (giuseppe.magazu@gmail.com)

## Contents

- [Description](#description)
- [ER Diagram](#er-diagram)
- [Entities](#entities)
  - [Ingredient](#ingredient)
  - [Speciality, Food, Drink](#speciality-food-drink)
  - [City](#city)
- [Relationships](#relationships)
  - [Contains](#contains)
  - [Variant](#variant)
  - [From](#from)
- [Packages](#packages)
  - [Controller](#controller)
  - [Exception](#exception)
  - [Model](#model)
  - [Repository](#repository)
  - [Service](#service)
- [Database](#database)
- [Environment](#environment)
- [Code Quality](#code-quality)
- [Installation](#installation)

## Description

Washoku (和食 - Japanese cuisine) is a Spring MVC web application that allows you to consult and manage Japanese culinary specialities.

Through to this application is possible to consult various informations regarding the different culinary specialities including the ingredients that compose it, the city of origin and the variants of a speciality, if present.

It is also possible to perform different searches that allow you to find certain specialities depending on the region, city of origin or with respect to the ingredients contained.

## ER Diagram

![ER diagram](images/er.png)

### Entities

#### Ingredient

The ingredient entity represents the ingredients that make up the various specialities described by the application. The entity is characterized by three attributes: Id, Name and Description (optional).

#### Speciality, Food, Drink

The speciality is the entity that represents the set of various Japanese specialities described by the application. The entity has two specializations: **Drink** and **Food**. Furthermore, the entity is characterized by three attributes: Id, Name and Description.

**Drink** is a specialization of the speciality entity representing the specialities of type drink. This entity inherits all the attributes from Speciality by adding an attribute (alcoholic) to highlight if the drink is alcoholic.

**Food** is a specialization of the speciality entity that represents the specialities of type food. This entity inherits all attributes from Speciality without adding any other attributes.

#### City

The city is the entity that represents the city of origin of the various specialities described by the application. The entity is characterized by three attributes: Id, Name and Region.

### Relationships

#### Contains

The Contains relationship is a Many-to-Many relationship that relates the specialities with the ingredients that compose it. There cannot be a speciality without at least one associated ingredient.

#### Variant

The Variant relationship is a One-to-Many, self-loop relationship that relates one speciality (original) to many specialities (variant) that describe the concept of variants of a speciality.

#### From

The From relationship is a Many-to-Many relationship that relates the specialities to the cities they come from. There cannot be a speciality without at least one associated city.

## Packages

The application is composed of five packages:

- com.assignment3.washoku.controller
- com.assignment3.washoku.exception
- com.assignment3.washoku.model
- com.assignment3.washoku.repository
- com.assignment3.washoku.service

### Controller

The controller package is responsible for delegating business logic to the service layer, accepting the input, and converting it to commands for the model or view.

The controller package contains the followings classes annotated with `@Controller`:

- CityController
- DrinkController
- FoodController
- IngredientController

A controller handles the followings requests:

| Method | Url                    | Description                                                                                                                                          |
|--------|------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------|
| GET    | /\<entity\>/             | Return a page with a list of items and allow to perform searches                                                                                     |
| GET    | /\<entity\>/details/{id} | Return a page with details about the item specified by id                                                                                            |
| GET    | /\<entity\>/add          | Return a page to insert a new item                                                                                                                   |
| GET    | /\<entity\>/edit/{id}    | Return a page to edit a new item                                                                                                                     |
| POST   | /\<entity\>/save         | Check validation then save the item passed and redirect to /\<entity\>/, in case validation is not satisfied return the previous page with errors |
| GET    | /\<entity\>/delete/{id}  | Delete the item by id and redirect to /\<entity\>/                                                                                                   |

The search operations are performed by getting the parameters in the query string via get requests. The parameters are optional and are therefore annotated in the following way `@RequestParam(required = false)`, in cases of parameters that are not of type String the default value is also specified. The parameters are then passed to the service methods.

In the save method the object passed in input is annotated with `@Valid` to trigger the validation.

Each method uses an instance of the ModelAndView class to specify the view name and passing model data to it.

### Exception

The exception package contains a global exception handler and custom exception definitions.

The classes are the followings:

- GlobalExceptionHandler
- CityNotFoundException
- DrinkNotFoundException
- FoodNotFoundException
- IngredientNotFoundException

The GlobalExceptionHandler class handles each type of exception and renders a view showing errors.

The other classes define a NotFoundException by extending the RuntimeException class, and these exceptions will be raised when the entity id is not found.

### Model

![Model Diagram](images/model-diagram.png)

The model package takes care of defining the application entities, by using the `@Entity` annotation on the domain classes.

The identifiers were managed according to a generation strategy `GenerationType.IDENTITY`, the "identity" indicates that the persistence provider must assign primary keys for the entity using a database identity column; an identity column is a column in a database table that is made up of values generated by the database.

The errors for each attribute have been described by also specifying various properties such as attributes that cannot be null and void via the `@NotBlank` annotation followed by the corresponding error message.

The getter and setter methods have been defined with the `@Getter` and `@Setter` annotation which automatically create these methods.

For inheritance, a `InheritanceType.SINGLE_TABLE` strategy was used which creates only one table for the hierarchy; therefore, it was necessary to differentiate the various records using a variable of type string defined through a discriminant `DiscriminatorType.STRING`.

A `FetchType.EAGER` data loading method is used, meaning the attribute data is immediately loaded into memory.

### Repository

![Repository Diagram](images/repository-diagram.png)

The repository package defines the basic crud operations and search operations on the entities. The interfaces IngredientRepository, CityRepository and SpecialityRepository extend [JpaRepository.](https://docs.spring.io/spring-data/jpa/docs/current/api/org/springframework/data/jpa/repository/JpaRepository.html)
Search operations were defined in each interface using the JPA APIs which translates method signatures into SQL queries.

All operations use case-sensitive attributes to give the user more freedom.

Search operations:

- FoodRepository
  - **findByCitiesNameIgnoreCase**: Return all foods from a certain city.
- DrinkRepository
  - **findByAlcoholicTrueAndCitiesRegionIgnoreCase**: Return all alcoholics from a certain region.
- IngredientRepository
  - **findBySpecialitiesNameIgnoreCase**: Return all ingredients of a certain speciality.

### Service

![Service Diagram](images/service-diagram.png)

The service package exposes a set of operations from the repository through Service classes and coordinates the repositories to handle more complex business logic.

A CrudService interface has been defined to provide CRUD methods (find, findAll, save, delete). For each repository a specific interface has been defined to provide a definition of the search methods. Each of these interfaces extend the CrudService and will be used to expose only the methods requested.

Each interface has been implemented by a class with the same name followed by the suffix 'Impl'.

A service may use additional repositories to perform more complex operations involving multiple entities.

The delete method of the CrudServiceImpl is overridden by each specific implementation. Before deleting an entity, the entities that optionally participate in the relationship are unlinked by setting values to null.

A search operation was defined inside the CityService and then implemented inside the CityServiceImpl:

- **findByRegionAndNumbersOfIngredients**: Return all cities per region that contain at least one speciality with less than a certain number of ingredients.

## Database

It's used an H2 database and saved in the folder `washoku/data/`

DDL generation is left to Hibernate by specifying `ddl-auto: create` in the configuration file application.yml. The 'create' option creates the schema and destroys the previous data in each run. In order to not re-create everything from scratch, it is necessary to change the option 'create' in 'update' after the first run.

An SQL script (H2 dialect) was provided to initialize the database `washoku/resources/init.sql`

Even though the primary keys are auto-incrementing integers, the script is supposed to be run only once. Because the keys have been specified to create relationships, running multiple times results in key duplication error.

## Environment

- Java 11
- Maven 3.6.3
- Intellij IDEA Ultimate 2020.3

## Code Quality

- Dependency Injection was done on constructors as suggested by the Spring documentation
- Lombok annotations were used to reduce the boilerplate code (getters, setters)
- SOLID principle was followed

- For the code style was used the [Google Java Style Guide](https://google.github.io/styleguide/javaguide.html)
- For code quality was used the following tools:
  - **FindBugs:** static code analyser which detects possible bugs in Java programs
  - **Checkstyle:** static code analysis tool for checking if Java source code is compliant with specified coding rules
  - **PMD:** static source code analyzer that reports on issues such as inefficient code or bad programming habits, which can reduce the performance and maintainability.
  - **Codacy:** automated code analysis/quality tool that the results can be found in the dashboard achieves static analysis, cyclomatic complexity and duplication. It runs after every commit, and the results can be found in the [dashboard](https://app.codacy.com/gl/saiteki-kai/2020-assignment3-washoku/dashboard)

## Installation

Clone the repository and enter inside the project folder

```shell
git clone git@gitlab.com:saiteki-kai/2020-assignment3-washoku.git
cd 2020-assignment3-washoku/washoku/
```

Compile and install

```shell
./mvnw clean install -Dmaven.test.skip=true   # unix
mvnw.cmd clean install -Dmaven.test.skip=true # windows
```

Run the application

```shell
./mvnw spring-boot:run   # unix
mvnw.cmd spring-boot:run # windows
```

The application will be available at [http://localhost:8080](http://localhost:8080)

<!--
    ./mvnw clean package spring-boot:repackage -Dmaven.test.skip=true
    java -Djava.security.egd=file:/dev/./urandom -jar target/washoku.jar
-->

<!--
After the first run, you can fill the database with some data

```shell
java -cp ./libs/h2*.jar org.h2.tools.RunScript -url jdbc:h2:file:./data/assignment3 -script ./src/main/resources/scripts/init.sql
```
-->
