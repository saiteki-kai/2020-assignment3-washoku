// Insert Specialties
INSERT INTO "speciality"("id", "name", "description", "alcoholic", "type", "original_id")
VALUES (1, 'Okonomiyaki',
        'Okonomiyaki is a pancake-like dish popular in various styles across Japan. In Osaka, shredded cabbage and a whole range of other ingredients such as squid, prawn, octopus or meat are mixed into a flour based batter and cooked before eaten with okonomiyaki sauce, mayonnaise, green laver (aonori) and dried bonito (katsuobushi). In some restaurants, okonomiyaki is prepared by the customers on a hot plate at their table.',
        NULL, 'Food', NULL);
INSERT INTO "speciality"("id", "name", "description", "alcoholic", "type", "original_id")
VALUES (2, 'Hiroshima-style Okonomiyaki',
        'Hiroshima''s most famous food is its own style of okonomiyaki. The local version of the dish is characterized by only a thin layer of batter and a generous amount of cabbage on top of yakisoba noodles. Popular toppings include oysters, squid and cheese. The dish is completed with bonito flakes, green laver and okonomiyaki sauce. Large numbers of okonomiyaki restaurants are found in Okonomimura and around Hiroshima Station. Diners are typically seated at a counter in front of the chef who prepares the meal on a large griddle.',
        NULL, 'Food', 1);
INSERT INTO "speciality"("id", "name", "description", "alcoholic", "type", "original_id")
VALUES (3, 'Matcha Tea',
        'Matcha, the deep-green, somewhat-bitter tea powder, has become an important element of Japanese culture because of the central role it plays in the centuries-old tea ceremony (sado).',
        FALSE, 'Drink', NULL);
INSERT INTO "speciality"("id", "name", "description", "alcoholic", "type", "original_id")
VALUES (4, 'Matcha Latte',
        'A matcha latte consists of matcha powder (made from the finely-ground leaves of certain green tea plants), water, and milk. If the matcha powder is unsweetened, the drink is then often sweetened with honey or syrup. The type of milk used will vary according to preference – as will the amount of milk used.',
        FALSE, 'Drink', 3);
INSERT INTO "speciality"("id", "name", "description", "alcoholic", "type", "original_id")
VALUES (5, 'Shochu',
        'Shochu is a clear, distilled liquor with an alcohol content of about 25%, that is popular across Japan, but especially in Kyushu. The shochu that is unique to Kagoshima is made from local sweet potatoes (Satsumaimo). Restaurants in Kagoshima will often have a variety of Satsuma Shochu on offer. It can be enjoyed straight, on the rocks (with ice) or mixed with hot water. Shochu is also used in cooking.',
        TRUE, 'Drink', NULL);

// Insert cities
INSERT INTO "city"("id", "name", "region")
VALUES (1, 'Tokyo', 'Honshu'),
       (2, 'Okinawa', 'Kyushu'),
       (3, 'Osaka', 'Kansai'),
       (4, 'Hiroshima', 'Chūgoku'),
       (5, 'Fukuoka', 'Kyushu'),
       (6, 'Kagoshima', 'Kyushu'),
       (7, 'Uji', 'Kansai'),
       (8, 'Nishio', 'Chūbu');

// Insert ingredients
INSERT INTO "ingredient"("id", "name", "description")
VALUES (1, 'salt', ''),
       (2, 'soy sauce',
        'Soy sauce is a fermented sauce made from soy, toasted wheat, water, salt and Aspergillus sp'),
       (3, 'green cabbage', 'A variety of Brassica oleracea plant species'),
       (4, 'carrot', ''),
       (5, 'onion', ''),
       (6, 'stalks celery',
        'Celery is a marshland plant in the family Apiaceae that has been cultivated as a vegetable since antiquity'),
       (7, 'water', ''),
       (8, 'matcha powder',
        'Ground powder of specially grown and processed green tea leaves, traditionally consumed in East Asia.'),
       (9, 'milk', ''),
       (10, 'syrup flavored',
        'Flavored syrups typically consist of a simple syrup, that is sugar (fully mixed with water while heated), with naturally occurring or artificial (synthesized) flavorings also dissolved in them. A sugar substitute may also be used.'),
       (11, 'rice', ''),
       (12, 'sweet potatoes', ''),
       (13, 'barley',
        'Barley (Hordeum vulgare), a member of the grass family, is a major cereal grain grown in temperate climates globally. It was one of the first cultivated grains, particularly in Eurasia as early as 10,000 years ago.'),
       (14, 'katsuobushi', 'Katsuobushi is a dried and fermented katsuo'),
       (15, 'yakisoba noodles', ''),
       (16, 'eggs', ''),
       (17, 'okonomiyaki sauce', '');

// Insert Speciality City
INSERT INTO "speciality_city" ("speciality_id", "city_id")
VALUES (1, 1),
       (1, 2),
       (2, 3),
       (2, 4),
       (3, 7),
       (3, 8),
       (4, 7),
       (4, 8),
       (5, 6);

// Insert SpecialityIngredient
INSERT INTO "speciality_ingredient" ("speciality_id", "ingredient_id")
VALUES (1, 1),
       (1, 2),
       (1, 3),
       (1, 4),
       (1, 5),
       (1, 6),
       (1, 7),
       (2, 3),
       (2, 5),
       (2, 14),
       (2, 15),
       (2, 16),
       (2, 17),
       (3, 7),
       (3, 8),
       (4, 7),
       (4, 8),
       (4, 9),
       (4, 10),
       (5, 11),
       (5, 12),
       (5, 13);

