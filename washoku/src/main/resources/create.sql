CREATE SEQUENCE IF NOT EXISTS hibernate_sequence;

// City Table
CREATE TABLE IF NOT EXISTS city
(
    id     int         NOT NULL,
    name   varchar(45) NOT NULL,
    region varchar(45) NOT NULL,
    PRIMARY KEY (id)
);

// Ingredient Table
CREATE TABLE IF NOT EXISTS ingredient
(
    id          int         NOT NULL,
    name        varchar(45) NOT NULL,
    description longtext    NOT NULL,
    PRIMARY KEY (id)
);

// Speciality Table
CREATE TABLE IF NOT EXISTS speciality
(
    id          int                    NOT NULL,
    name        varchar(45)            NOT NULL,
    description longtext               NOT NULL,
    type        enum ('Drink', 'Food') NOT NULL,
    alcoholic   tinyint DEFAULT NULL,
    original_id int     DEFAULT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_variant_speciality FOREIGN KEY (original_id) REFERENCES speciality (id) ON DELETE SET NULL ON UPDATE SET NULL
);

// Speciality-Ingredient Relationship
CREATE TABLE IF NOT EXISTS speciality_ingredient
(
    speciality_id int NOT NULL,
    ingredient_id int NOT NULL,
    PRIMARY KEY (speciality_id, ingredient_id),
    CONSTRAINT fk_contains_ingredient FOREIGN KEY (ingredient_id) REFERENCES ingredient (id) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT fk_contains_speciality FOREIGN KEY (speciality_id) REFERENCES speciality (id) ON DELETE CASCADE ON UPDATE CASCADE
);

// Speciality-City Relationship
CREATE TABLE IF NOT EXISTS speciality_city
(
    city_id       int NOT NULL,
    speciality_id int NOT NULL,
    PRIMARY KEY (city_id, speciality_id),
    CONSTRAINT fk_from_city FOREIGN KEY (city_id) REFERENCES city (id) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT fk_from_speciality FOREIGN KEY (speciality_id) REFERENCES speciality (id) ON DELETE CASCADE ON UPDATE CASCADE
);
