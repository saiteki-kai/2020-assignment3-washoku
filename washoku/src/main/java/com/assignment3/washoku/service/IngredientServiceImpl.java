package com.assignment3.washoku.service;

import com.assignment3.washoku.model.Ingredient;
import com.assignment3.washoku.model.Speciality;
import com.assignment3.washoku.repository.IngredientRepository;
import com.assignment3.washoku.repository.SpecialityRepository;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public class IngredientServiceImpl extends CrudServiceImpl<Ingredient>
    implements IngredientService {

  private final SpecialityRepository<Speciality> specialityRepository;

  public IngredientServiceImpl(
      IngredientRepository ingredientRepository,
      SpecialityRepository<Speciality> specialityRepository) {
    super(ingredientRepository);
    this.specialityRepository = specialityRepository;
  }

  @Override
  public List<Ingredient> findBySpecialityName(String name) {
    return ((IngredientRepository) repository).findBySpecialitiesNameIgnoreCase(name);
  }

  @Override
  public void delete(Ingredient ingredient) {
    // delete all specialties with no ingredients left
    ingredient
        .getSpecialities()
        .forEach(
            s -> {
              s.getIngredients().remove(ingredient);
              if (s.getIngredients().isEmpty()) {
                specialityRepository.deleteById(s.getId());
              }
            });

    super.delete(ingredient);
  }
}
