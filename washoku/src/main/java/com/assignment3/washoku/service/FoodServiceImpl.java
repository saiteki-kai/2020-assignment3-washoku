package com.assignment3.washoku.service;

import com.assignment3.washoku.model.Food;
import com.assignment3.washoku.repository.FoodRepository;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public class FoodServiceImpl extends SpecialityServiceImpl<Food> implements FoodService {

  public FoodServiceImpl(FoodRepository repository) {
    super(repository);
  }

  @Override
  public List<Food> findByCityName(String name) {
    return ((FoodRepository) repository).findByCitiesNameIgnoreCase(name);
  }
}
