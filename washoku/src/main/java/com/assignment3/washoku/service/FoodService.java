package com.assignment3.washoku.service;

import com.assignment3.washoku.model.Food;
import java.util.List;

public interface FoodService extends CrudService<Food> {
  List<Food> findByCityName(String name);
}
