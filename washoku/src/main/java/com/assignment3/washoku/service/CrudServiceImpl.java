package com.assignment3.washoku.service;

import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public abstract class CrudServiceImpl<T> implements CrudService<T> {

  protected JpaRepository<T, Long> repository;

  protected CrudServiceImpl(JpaRepository<T, Long> repository) {
    this.repository = repository;
  }

  @Override
  public List<T> findAll() {
    return repository.findAll();
  }

  @Override
  public Optional<T> find(Long id) {
    return repository.findById(id);
  }

  @Override
  public void save(T entity) {
    repository.save(entity);
  }

  @Override
  public void delete(T entity) {
    repository.delete(entity);
  }
}
