package com.assignment3.washoku.service;

import com.assignment3.washoku.model.City;
import java.util.List;

public interface CityService extends CrudService<City> {
  List<City> findAllByRegionAndNumbersOfIngredients(String region, int numberOfIngredients);
}
