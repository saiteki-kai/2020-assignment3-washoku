package com.assignment3.washoku.service;

import com.assignment3.washoku.model.Ingredient;
import java.util.List;

public interface IngredientService extends CrudService<Ingredient> {
  List<Ingredient> findBySpecialityName(String name);
}
