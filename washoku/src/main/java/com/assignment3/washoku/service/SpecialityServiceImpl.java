package com.assignment3.washoku.service;

import com.assignment3.washoku.model.Speciality;
import com.assignment3.washoku.repository.SpecialityRepository;

public abstract class SpecialityServiceImpl<T extends Speciality> extends CrudServiceImpl<T> {

  protected SpecialityServiceImpl(SpecialityRepository<T> repository) {
    super(repository);
  }

  @Override
  public void delete(T speciality) {
    // unlink VariantOriginal relationship
    speciality.setOriginal(null);
    speciality.getVariants().forEach(v -> v.setOriginal(null));

    // unlink SpecialityCity relationship
    speciality.getCities().forEach(c -> c.getSpecialities().remove(speciality));

    super.delete(speciality);
  }
}
