package com.assignment3.washoku.service;

import java.util.List;
import java.util.Optional;

public interface CrudService<T> {
  List<T> findAll();

  Optional<T> find(Long id);

  void save(T entity);

  void delete(T entity);
}
