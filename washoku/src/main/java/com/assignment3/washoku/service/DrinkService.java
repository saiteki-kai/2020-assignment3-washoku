package com.assignment3.washoku.service;

import com.assignment3.washoku.model.Drink;
import java.util.List;

public interface DrinkService extends CrudService<Drink> {
  List<Drink> findAlcoholicsByRegion(String regionName);
}
