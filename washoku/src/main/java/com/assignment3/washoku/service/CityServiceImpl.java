package com.assignment3.washoku.service;

import com.assignment3.washoku.model.City;
import com.assignment3.washoku.model.Speciality;
import com.assignment3.washoku.repository.CityRepository;
import com.assignment3.washoku.repository.SpecialityRepository;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;

@Service
public class CityServiceImpl extends CrudServiceImpl<City> implements CityService {

  private final SpecialityRepository<Speciality> specialityRepository;

  public CityServiceImpl(
      CityRepository cityRepository, SpecialityRepository<Speciality> specialityRepository) {
    super(cityRepository);
    this.specialityRepository = specialityRepository;
  }

  @Override
  public List<City> findAllByRegionAndNumbersOfIngredients(String region, int numberOfIngredients) {
    return repository.findAll().stream()
        .filter(x -> x.getRegion().equalsIgnoreCase(region))
        .filter(
            x ->
                x.getSpecialities().stream()
                    .anyMatch(s -> s.getIngredients().size() <= numberOfIngredients))
        .collect(Collectors.toList());
  }

  @Override
  public void delete(City city) {
    // delete all specialties with no cities left
    city.getSpecialities()
        .forEach(
            s -> {
              s.getCities().remove(city);
              if (s.getCities().isEmpty()) {
                specialityRepository.deleteById(s.getId());
              }
            });

    super.delete(city);
  }
}
