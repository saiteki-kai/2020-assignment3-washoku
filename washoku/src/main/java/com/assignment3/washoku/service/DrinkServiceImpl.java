package com.assignment3.washoku.service;

import com.assignment3.washoku.model.Drink;
import com.assignment3.washoku.repository.DrinkRepository;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public class DrinkServiceImpl extends SpecialityServiceImpl<Drink> implements DrinkService {

  public DrinkServiceImpl(DrinkRepository repository) {
    super(repository);
  }

  public List<Drink> findAlcoholicsByRegion(String region) {
    return ((DrinkRepository) repository).findByAlcoholicTrueAndCitiesRegionIgnoreCase(region);
  }
}
