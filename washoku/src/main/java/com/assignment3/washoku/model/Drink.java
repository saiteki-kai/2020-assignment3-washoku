package com.assignment3.washoku.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@DiscriminatorValue("Drink")
public class Drink extends Speciality {

  @Column private boolean alcoholic;
}
