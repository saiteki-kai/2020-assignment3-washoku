package com.assignment3.washoku.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("Food")
public class Food extends Speciality {}
