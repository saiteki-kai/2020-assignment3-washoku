package com.assignment3.washoku.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import java.util.HashSet;
import java.util.Set;
import org.hibernate.validator.constraints.Length;

@Getter
@Setter
@Entity
@Table(name = "city")
public class City {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @NotBlank(message = "must not be blank")
  @Length(max = 255, message = "must be max 255 characters")
  @Column(nullable = false)
  private String name;

  @NotBlank(message = "must not be blank")
  @Length(max = 255, message = "must be max 255 characters")
  @Column(nullable = false)
  private String region;

  @ManyToMany(mappedBy = "cities", fetch = FetchType.EAGER)
  private Set<Speciality> specialities = new HashSet<>();
}
