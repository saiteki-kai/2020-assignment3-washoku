package com.assignment3.washoku.model;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

@Getter
@Setter
@Entity
@Table(name = "speciality")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorValue("Speciality")
@DiscriminatorColumn(name = "type", discriminatorType = DiscriminatorType.STRING)
public class Speciality {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @NotBlank(message = "must not be blank")
  @Length(max = 255, message = "must be max 255 characters")
  @Column(nullable = false)
  private String name;

  @NotBlank(message = "must not be blank")
  @Column(nullable = false, columnDefinition = "TEXT")
  private String description;

  @Column(insertable = false, updatable = false)
  private String type;

  @ManyToOne
  @JoinColumn(name = "original_id")
  private Speciality original;

  @OneToMany(mappedBy = "original")
  private Set<Speciality> variants = new HashSet<>();

  @NotEmpty(message = "there must be at least one city")
  @ManyToMany(fetch = FetchType.EAGER)
  @JoinTable(
      name = "speciality_city",
      joinColumns = @JoinColumn(name = "speciality_id", nullable = false),
      inverseJoinColumns = @JoinColumn(name = "city_id", nullable = false))
  private Set<City> cities = new HashSet<>();

  @NotEmpty(message = "there must be at least one ingredient")
  @ManyToMany(fetch = FetchType.EAGER)
  @JoinTable(
      name = "speciality_ingredient",
      joinColumns = @JoinColumn(name = "speciality_id", nullable = false),
      inverseJoinColumns = @JoinColumn(name = "ingredient_id", nullable = false))
  private Set<Ingredient> ingredients = new HashSet<>();

  protected Speciality() {}
}
