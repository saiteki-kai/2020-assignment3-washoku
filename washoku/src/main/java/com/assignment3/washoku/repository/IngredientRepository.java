package com.assignment3.washoku.repository;

import com.assignment3.washoku.model.Ingredient;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IngredientRepository extends JpaRepository<Ingredient, Long> {
  List<Ingredient> findBySpecialitiesNameIgnoreCase(String name);
}
