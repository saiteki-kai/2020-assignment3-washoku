package com.assignment3.washoku.repository;

import com.assignment3.washoku.model.Drink;
import java.util.List;
import org.springframework.stereotype.Repository;

@Repository
public interface DrinkRepository extends SpecialityRepository<Drink> {
  List<Drink> findByAlcoholicTrueAndCitiesRegionIgnoreCase(String region);
}
