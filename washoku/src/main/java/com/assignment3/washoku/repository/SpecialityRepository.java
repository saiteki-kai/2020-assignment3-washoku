package com.assignment3.washoku.repository;

import com.assignment3.washoku.model.Speciality;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SpecialityRepository<T extends Speciality> extends JpaRepository<T, Long> {}
