package com.assignment3.washoku.repository;

import com.assignment3.washoku.model.Food;
import java.util.List;
import org.springframework.stereotype.Repository;

@Repository
public interface FoodRepository extends SpecialityRepository<Food> {
  List<Food> findByCitiesNameIgnoreCase(String name);
}
