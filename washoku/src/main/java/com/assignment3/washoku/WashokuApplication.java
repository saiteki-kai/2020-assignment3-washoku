package com.assignment3.washoku;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WashokuApplication {

  public static void main(String[] args) {
    SpringApplication.run(WashokuApplication.class, args);
  }
}
