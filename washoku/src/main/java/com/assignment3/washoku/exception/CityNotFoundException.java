package com.assignment3.washoku.exception;

public class CityNotFoundException extends RuntimeException {
  public CityNotFoundException(Long id) {
    super("City id not found : " + id);
  }
}
