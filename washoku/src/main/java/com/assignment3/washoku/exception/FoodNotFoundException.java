package com.assignment3.washoku.exception;

public class FoodNotFoundException extends RuntimeException {
  public FoodNotFoundException(Long id) {
    super("Food id not found : " + id);
  }
}
