package com.assignment3.washoku.exception;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class GlobalExceptionHandler {

  @ExceptionHandler(Exception.class)
  public ModelAndView handleError(HttpServletRequest req, Exception ex) {
    String msg = ex.getMessage();
    String errorMsg = msg.isEmpty() ? "Unexpected Error" : msg;

    ModelAndView mv = new ModelAndView("error");
    mv.addObject("url", req.getHeader("referer"));
    mv.addObject("errorMsg", errorMsg);

    return mv;
  }
}
