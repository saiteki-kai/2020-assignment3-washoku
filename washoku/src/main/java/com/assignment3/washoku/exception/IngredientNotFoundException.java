package com.assignment3.washoku.exception;

public class IngredientNotFoundException extends RuntimeException {
  public IngredientNotFoundException(Long id) {
    super("Ingredient id not found : " + id);
  }
}
