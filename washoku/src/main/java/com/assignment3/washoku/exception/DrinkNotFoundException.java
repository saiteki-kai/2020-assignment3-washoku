package com.assignment3.washoku.exception;

public class DrinkNotFoundException extends RuntimeException {
  public DrinkNotFoundException(Long id) {
    super("Drink id not found : " + id);
  }
}
