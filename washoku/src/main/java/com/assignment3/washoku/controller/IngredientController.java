package com.assignment3.washoku.controller;

import com.assignment3.washoku.exception.IngredientNotFoundException;
import com.assignment3.washoku.model.Ingredient;
import com.assignment3.washoku.service.IngredientService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/ingredients")
public class IngredientController {

  private static final String VIEW_INDEX = "pages/ingredients/index";
  private static final String VIEW_DETAILS = "pages/ingredients/details";
  private static final String VIEW_SAVE = "pages/ingredients/save";
  private static final String REDIRECT = "redirect:/ingredients";

  private final IngredientService ingredientService;

  public IngredientController(IngredientService ingredientService) {
    this.ingredientService = ingredientService;
  }

  private ModelAndView formView(Ingredient ingredient) {
    ModelAndView mv = new ModelAndView(VIEW_SAVE);
    mv.addObject("ingredient", ingredient);
    return mv;
  }

  @GetMapping
  public ModelAndView getIngredients(@RequestParam(required = false) String specialityName) {
    ModelAndView mv = new ModelAndView(VIEW_INDEX);

    List<Ingredient> ingredients;
    if (specialityName == null || specialityName.isEmpty()) {
      ingredients = ingredientService.findAll();
    } else {
      ingredients = ingredientService.findBySpecialityName(specialityName);
    }

    mv.addObject("ingredients", ingredients);
    mv.addObject("specialityName", specialityName);
    return mv;
  }

  @GetMapping("/details/{id}")
  public ModelAndView getIngredient(@PathVariable("id") Long id) {
    ModelAndView mv = new ModelAndView(VIEW_DETAILS);

    Ingredient ingredient =
        ingredientService.find(id).orElseThrow(() -> new IngredientNotFoundException(id));

    mv.addObject("ingredient", ingredient);
    mv.addObject("specialities", ingredient.getSpecialities());
    return mv;
  }

  @GetMapping("/add")
  public ModelAndView addIngredient() {
    return formView(new Ingredient());
  }

  @GetMapping("/edit/{id}")
  public ModelAndView editIngredient(@PathVariable("id") Long id) {
    Ingredient ingredient =
        ingredientService.find(id).orElseThrow(() -> new IngredientNotFoundException(id));
    return formView(ingredient);
  }

  @PostMapping("/save")
  public ModelAndView saveIngredient(@Valid Ingredient ingredient, BindingResult result) {
    if (result.hasErrors()) {
      return formView(ingredient);
    }

    ingredientService.save(ingredient);
    return new ModelAndView(REDIRECT);
  }

  @GetMapping(value = "/delete/{id}")
  public ModelAndView deleteIngredient(@PathVariable("id") Long id) {
    Ingredient ingredient =
        ingredientService.find(id).orElseThrow(() -> new IngredientNotFoundException(id));
    ingredientService.delete(ingredient);
    return new ModelAndView(REDIRECT);
  }
}
