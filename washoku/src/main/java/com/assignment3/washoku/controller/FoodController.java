package com.assignment3.washoku.controller;

import com.assignment3.washoku.exception.FoodNotFoundException;
import com.assignment3.washoku.model.Food;
import com.assignment3.washoku.service.CityService;
import com.assignment3.washoku.service.FoodService;
import com.assignment3.washoku.service.IngredientService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/foods")
public class FoodController {

  private static final String VIEW_INDEX = "pages/foods/index";
  private static final String VIEW_DETAILS = "pages/foods/details";
  private static final String VIEW_SAVE = "pages/foods/save";
  private static final String REDIRECT = "redirect:/foods";

  private final FoodService foodService;
  private final CityService cityService;
  private final IngredientService ingredientService;

  public FoodController(
      FoodService foodService, CityService cityService, IngredientService ingredientService) {
    this.foodService = foodService;
    this.cityService = cityService;
    this.ingredientService = ingredientService;
  }

  private ModelAndView formView(Food food) {
    ModelAndView mv = new ModelAndView(VIEW_SAVE);

    List<Food> foods = foodService.findAll();
    foods.remove(food); // exclude food from selectable original foods

    mv.addObject("food", food);
    mv.addObject("foods", foods);
    mv.addObject("_cities", cityService.findAll());
    mv.addObject("_ingredients", ingredientService.findAll());
    return mv;
  }

  @GetMapping
  public ModelAndView getFoods(@RequestParam(required = false) String cityName) {
    ModelAndView mv = new ModelAndView(VIEW_INDEX);

    List<Food> foods;
    if (cityName == null || cityName.isEmpty()) {
      foods = foodService.findAll();
    } else {
      foods = foodService.findByCityName(cityName);
    }

    mv.addObject("foods", foods);
    mv.addObject("cityName", cityName);
    return mv;
  }

  @GetMapping("/details/{id}")
  public ModelAndView getFood(@PathVariable("id") Long id) {
    Food food = foodService.find(id).orElseThrow(() -> new FoodNotFoundException(id));

    ModelAndView mv = new ModelAndView(VIEW_DETAILS);
    mv.addObject("food", food);
    mv.addObject("cities", food.getCities());
    mv.addObject("ingredients", food.getIngredients());
    return mv;
  }

  @GetMapping("/add")
  public ModelAndView addFood() {
    return formView(new Food());
  }

  @GetMapping("/edit/{id}")
  public ModelAndView editFood(@PathVariable("id") Long id) {
    Food food = foodService.find(id).orElseThrow(() -> new FoodNotFoundException(id));
    return formView(food);
  }

  @PostMapping("/save")
  public ModelAndView saveFood(@Valid Food food, BindingResult result) {
    if (result.hasErrors()) {
      return formView(food);
    }

    foodService.save(food);
    return new ModelAndView(REDIRECT);
  }

  @GetMapping("/delete/{id}")
  public ModelAndView deleteFood(@PathVariable("id") Long id) {
    Food food = foodService.find(id).orElseThrow(() -> new FoodNotFoundException(id));
    foodService.delete(food);
    return new ModelAndView(REDIRECT);
  }
}
