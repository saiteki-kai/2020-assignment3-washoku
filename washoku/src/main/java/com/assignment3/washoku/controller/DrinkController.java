package com.assignment3.washoku.controller;

import com.assignment3.washoku.exception.DrinkNotFoundException;
import com.assignment3.washoku.model.Drink;
import com.assignment3.washoku.service.CityService;
import com.assignment3.washoku.service.DrinkService;
import com.assignment3.washoku.service.IngredientService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/drinks")
public class DrinkController {

  private static final String VIEW_INDEX = "pages/drinks/index";
  private static final String VIEW_DETAILS = "pages/drinks/details";
  private static final String VIEW_SAVE = "pages/drinks/save";
  private static final String REDIRECT = "redirect:/drinks";

  private final DrinkService drinkService;
  private final CityService cityService;
  private final IngredientService ingredientService;

  public DrinkController(
      DrinkService drinkService, CityService cityService, IngredientService ingredientService) {
    this.drinkService = drinkService;
    this.cityService = cityService;
    this.ingredientService = ingredientService;
  }

  private ModelAndView formView(Drink drink) {
    ModelAndView mv = new ModelAndView(VIEW_SAVE);

    List<Drink> drinks = drinkService.findAll();
    drinks.remove(drink); // exclude drink from selectable original drinks

    mv.addObject("drink", drink);
    mv.addObject("drinks", drinks);
    mv.addObject("_cities", cityService.findAll());
    mv.addObject("_ingredients", ingredientService.findAll());
    return mv;
  }

  @GetMapping
  public ModelAndView getDrinks(@RequestParam(required = false) String region) {
    ModelAndView mv = new ModelAndView(VIEW_INDEX);

    List<Drink> drinks;
    if (region == null || region.isEmpty()) {
      drinks = drinkService.findAll();
    } else {
      drinks = drinkService.findAlcoholicsByRegion(region);
    }

    mv.addObject("drinks", drinks);
    mv.addObject("region", region);
    return mv;
  }

  @GetMapping("/details/{id}")
  public ModelAndView getDrink(@PathVariable("id") Long id) {
    ModelAndView mv = new ModelAndView(VIEW_DETAILS);

    Drink drink = drinkService.find(id).orElseThrow(() -> new DrinkNotFoundException(id));

    mv.addObject("drink", drink);
    mv.addObject("cities", drink.getCities());
    mv.addObject("ingredients", drink.getIngredients());
    return mv;
  }

  @GetMapping("/add")
  public ModelAndView addDrink() {
    return formView(new Drink());
  }

  @GetMapping("/edit/{id}")
  public ModelAndView editDrink(@PathVariable("id") Long id) {
    Drink drink = drinkService.find(id).orElseThrow(() -> new DrinkNotFoundException(id));
    return formView(drink);
  }

  @PostMapping("/save")
  public ModelAndView saveDrink(@Valid Drink drink, BindingResult result) {
    if (result.hasErrors()) {
      return formView(drink);
    }

    drinkService.save(drink);
    return new ModelAndView(REDIRECT);
  }

  @GetMapping("/delete/{id}")
  public ModelAndView deleteDrink(@PathVariable("id") Long id) {
    Drink drink = drinkService.find(id).orElseThrow(() -> new DrinkNotFoundException(id));
    drinkService.delete(drink);
    return new ModelAndView(REDIRECT);
  }
}
