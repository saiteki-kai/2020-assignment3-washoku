package com.assignment3.washoku.controller;

import com.assignment3.washoku.exception.CityNotFoundException;
import com.assignment3.washoku.model.City;
import com.assignment3.washoku.service.CityService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/cities")
public class CityController {

  private static final String VIEW_INDEX = "pages/cities/index";
  private static final String VIEW_DETAILS = "pages/cities/details";
  private static final String VIEW_SAVE = "pages/cities/save";
  private static final String REDIRECT = "redirect:/cities";

  private final CityService cityService;

  public CityController(CityService cityService) {
    this.cityService = cityService;
  }

  private ModelAndView formView(City city) {
    ModelAndView mv = new ModelAndView(VIEW_SAVE);
    mv.addObject("city", city);
    return mv;
  }

  @GetMapping
  public ModelAndView getCities(
      @RequestParam(required = false) String cityRegion,
      @RequestParam(required = false, defaultValue = "0") int numberOfIngredients) {
    ModelAndView mv = new ModelAndView(VIEW_INDEX);

    List<City> cities;
    if (cityRegion == null || cityRegion.isEmpty() || numberOfIngredients <= 0) {
      cities = cityService.findAll();
    } else {
      cities = cityService.findAllByRegionAndNumbersOfIngredients(cityRegion, numberOfIngredients);
    }

    mv.addObject("cities", cities);
    mv.addObject("cityRegion", cityRegion);
    mv.addObject("numberOfIngredients", numberOfIngredients);
    return mv;
  }

  @GetMapping("/details/{id}")
  public ModelAndView getCity(@PathVariable("id") Long id) {
    ModelAndView mv = new ModelAndView(VIEW_DETAILS);

    City city = cityService.find(id).orElseThrow(() -> new CityNotFoundException(id));

    mv.addObject("city", city);
    mv.addObject("specialities", city.getSpecialities());
    return mv;
  }

  @GetMapping("/add")
  public ModelAndView addCity() {
    return formView(new City());
  }

  @GetMapping("/edit/{id}")
  public ModelAndView editCity(@PathVariable("id") Long id) {
    City city = cityService.find(id).orElseThrow(() -> new CityNotFoundException(id));
    return formView(city);
  }

  @PostMapping("/save")
  public ModelAndView saveCity(@Valid City city, BindingResult result) {
    if (result.hasErrors()) {
      return formView(city);
    }

    cityService.save(city);
    return new ModelAndView(REDIRECT);
  }

  @GetMapping(value = "/delete/{id}")
  public ModelAndView deleteCity(@PathVariable("id") Long id) {
    City city = cityService.find(id).orElseThrow(() -> new CityNotFoundException(id));
    cityService.delete(city);
    return new ModelAndView(REDIRECT);
  }
}
